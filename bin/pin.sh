#!/bin/bash
#
# ipfs-pinlist-cdn
# https://github.com/sneak/ipfs-pinlist-cdn/
# author: Jeffrey Paul <sneak@sneak.berlin>

SNEAKID="QmaN64WRYdHBojWFQRLxkdjtX6TEnqnCq8uAugpyJJpCVp"
RESOURCE="$(ipfs resolve /ipns/$SNEAKID/pinlist/pinlist.txt)"

# i know that ipfs pin add can read stdin
# but sometimes it lags pretty badly or freezes
# and i think invoking it for each pin is hopefully
# going to be more reliable.

# i have setup this regex so that hopefully
# no matter what i put in the pinlist file it doesn't
# result in RCE on your box.

# first add normal ipfs resource pins
for IPFSPATH in $(
    ipfs cat $RESOURCE |
    egrep '^\/ip[fn]s\/[a-zA-Z0-9\-\_\/\.]{5,70}$' |
    egrep '^\/ipfs\/'
); do
    ipfs pin add $IPFSPATH
done

# then resolve ipns names in pinlist and pin those targets too
for IPNSPATH in $(
    ipfs cat $RESOURCE |
    egrep '^\/ip[fn]s\/[a-zA-Z0-9\-\_\/\.]{5,70}$' |
    egrep '^\/ipns\/'
); do
    echo $IPNSPATH | ipfs resolve | ipfs pin add
done
