IPFS_ROOT := $(HOME)/Documents/sneak/my-ipfs-root
PINLIST_DIR := $(IPFS_ROOT)/pinlist

default: deploy

.PHONY: deploy clean

clean:
	rm -f pinlist.txt pinlist.new

deploy: pinlist.txt
	mkdir -p $(PINLIST_DIR)
	cat ./pinlist.txt > $(PINLIST_DIR)/pinlist.txt
	cat ./bin/pin.sh > $(PINLIST_DIR)/pin.sh
	cat ./README.md > $(PINLIST_DIR)/README.md
	ipfs add -r $(IPFS_ROOT) | \
		tail -1 | \
		awk -F' ' '{print $$2}' | \
		ipfs name publish

pinlist.txt: pins/*.json
	jq -r .pins[] $^ > pinlist.new && mv pinlist.new $@.tmp
	echo "# timestamp=$$(date -u +%s)" >> $@.tmp
	cat $@.tmp | sort | uniq > $@
	rm $@.tmp

tidy:
	for X in pins/*.json ; do \
		echo $$X ; \
		jq -S --indent 4 . $$X > $$X.new && mv $$X.new $$X ; \
	done
