# ipfs-pinlist-cdn

I want more stuff to get pinned on more IPFS hosts.

This is a repo that generates an input file for a script that can be run
from cron to pin a bunch of IPFS paths.  The list itself will be
periodically generated from this repo and placed into ipfs under my
namespace, allowing people to fetch it consistently and automatically update
their pinning lists.

# How To Use

```

```

# Adding Content

https://github.com/sneak/ipfs-pinlist-cdn

# Requirements For Inclusion

* general-purpose interest
* not too big (blogs, metadata, documents)
    * try to keep it under 1-200MiB
* not general purpose software mirrors / etc
    * maybe later

# Author

* Jeffrey Paul <sneak@sneak.berlin>
* https://github.com/sneak
* https://twitter.com/sneakdotberlin
* http://localhost:8080/ipns/QmaN64WRYdHBojWFQRLxkdjtX6TEnqnCq8uAugpyJJpCVp/
